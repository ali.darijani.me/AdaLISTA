import numpy as np
import matplotlib.pyplot as plt
import train_URWF_old
import train_UIRWF_old
import params


def plot():
    plt.rcParams["text.usetex"] = True
    plt.title("err vs epoch index")
    plt.xlabel("$epochs$")
    plt.ylabel(r"$\min \| z_{i}e^{-i\phi} - x_i \|_F$", fontsize=14, color="k")
    plt.figure(1)
    plt.semilogy(
        np.arange(0, params.EPOCHS),
        train_URWF_old.err_URWF_train,
        color="red",
        label="URWF on train",
    )
    plt.semilogy(
        np.arange(0, params.EPOCHS),
        train_URWF_old.err_URWF_val,
        color="blue",
        label="URWF on test",
    )
    plt.semilogy(
        np.arange(0, params.EPOCHS),
        train_UIRWF_old.err_UIRWF_train,
        color="orange",
        label="UIRWF on train",
    )
    plt.semilogy(
        np.arange(0, params.EPOCHS),
        train_UIRWF_old.err_UIRWF_val,
        color="green",
        label="UIRWF on test",
    )
    plt.title(r"URWF vs UIRWF on train/test data", fontsize=10)
    plt.legend()
    plt.savefig(
        "Scenario = "
        + str(params.scenario)
        + ", Layers = "
        + str(params.T)
        + ", Epochs = "
        + str(params.EPOCHS)
        + "_ LR = "
        + str(params.LR)
        + "_Learning_Process.pdf"
    )
    # plt.show()
