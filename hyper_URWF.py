"""
Optuna example that optimizes multi-layer perceptrons using PyTorch.

In this example, we optimize the validation accuracy of fashion product recognition using
PyTorch and FashionMNIST. We optimize the neural network architecture as well as the optimizer
configuration. As it is too time consuming to use the whole FashionMNIST dataset,
we here use a small subset of it.

"""
import generating

import numpy as np
import math
import os
import train_URWF
import optuna
from optuna.trial import TrialState
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
from torchvision import datasets
from torchvision import transforms


def objective(trial):
    # SCENARIO = trial.suggest_categorical(
    # "SCENARIO",
    # ["scalar", "vector", "matrix", "vector-matrix", "tensor", "SPD-matrix"],
    # )
    LR = trial.suggest_float("LR", 1e-4, 1e-2)
    scenario = trial.suggest_int("scenario", 0, 7)
    x, x_val, Amatrix, y, y_val = generating.gen_x_Amatrix_y(
        n=64, m=10 * 64, N=100, cplx_flag=1, cuda_flag=1
    )

    return train_URWF.train_URWF(
        x,
        x_val,
        Amatrix,
        y,
        y_val,
        SCENARIO=scenario,
        LR=LR,
        cuda_flag=1,
        cplx_flag=1,
        npower_iter=30,
        T=30,
        scalar=True,
        vector=True,
        matrix=True,
        tensor=True,
        EPOCHS=50,
    )


if __name__ == "__main__":
    # print("enter 0 for Wirtinger Flow")
    # print("enter 1 for Truncated Wirtinger Flow")
    # print("enter 2 for Reshaped Wirtinger Flow")
    # print("enter 3 for Incrementally Reshaped Wirtinger Flow")
    # print("enter 4 for Minibatch Incrementally Reshaped Wirtinger Flow")
    # name = input(
    # "Please Enter Your Desired Deep Unfolding Variant for Hyperparameter Study: "
    # )
    study = optuna.create_study(
        direction="minimize",
        storage="sqlite:///db.URWF",
        study_name="Deep Unfolding Using Reshaped Wirtinger Flow",
        load_if_exists=True,
    )
    study.optimize(objective, n_trials=20)

    pruned_trials = study.get_trials(deepcopy=False, states=[TrialState.PRUNED])
    complete_trials = study.get_trials(deepcopy=False, states=[TrialState.COMPLETE])

    print("Study statistics: ")
    print("  Number of finished trials: ", len(study.trials))
    print("  Number of pruned trials: ", len(pruned_trials))
    print("  Number of complete trials: ", len(complete_trials))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: ", trial.value)

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))
