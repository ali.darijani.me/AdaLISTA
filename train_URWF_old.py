import torch
import params
import models
import generating
import numpy as np
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from datetime import datetime
import matplotlib.pyplot as plt


def train_URWF(x, x_val, Amatrix, y, y_val, SCENARIO, LR, DEVICE):
    data = torch.cat([x, y], dim=1)
    n_samples = data.shape[0]
    n_val = int(0.1 * n_samples)
    shuffled_indices = torch.randperm(n_samples)
    train_indices = shuffled_indices[:-n_val]
    val_indices = shuffled_indices[-n_val:]
    data_train = data[train_indices]
    data_val = data[val_indices]
    training_loader = DataLoader(data_train, batch_size=2, shuffle=True)
    validation_loader = DataLoader(data_val, batch_size=2, shuffle=False)
    model_URWF = models.URWF()
    # model_UIRWF = models.UIRWF()
    err_URWF_train = np.zeros(params.EPOCHS)
    err_URWF_val = np.zeros(params.EPOCHS)

    # err_URWF_knot = np.zeros(params.N_train)
    # err_URWF_inf = np.zeros(params.N_train)
    # err_UIRWF_train = np.zeros(params.EPOCHS)
    # err_UIRWF_val = np.zeros(params.EPOCHS)
    # Optimizers specified in the torch.optim package
    optimizer = torch.optim.Adam(model_URWF.parameters(), amsgrad=True, lr=params.LR)

    def train_one_epoch_URWF(epoch_index, tb_writer, data_train, data_val):
        running_loss = 0.0
        last_loss = 0.0
        x_split_val = data_val[:, 0 : params.n]
        y_split_val = torch.abs(data_val[:, params.n :])
        x_pred_val = model_URWF(x_split_val.T, y_split_val.T, Amatrix)
        loss_val = models.my_loss(x_split_val.T, x_pred_val)
        err_URWF_val[epoch_index] = loss_val
        x_split_train = data_train[:, 0 : params.n]
        y_split_train = torch.abs(data_train[:, params.n :])
        x_pred_train = model_URWF(x_split_train.T, y_split_train.T, Amatrix)
        loss_train = models.my_loss(x_split_train.T, x_pred_train)
        err_URWF_train[epoch_index] = loss_train
        print("  loss_train_URWF {} loss_val_URWF: {}".format(loss_train, loss_val))
        # Here, we use enumerate(training_loader) instead of
        # iter(training_loader) so that we can track the batch
        # index and do some intra-epoch reporting
        for i, data in enumerate(training_loader):
            # Every data instance is an input + label pair
            # inputs, labels = data
            x_split = data_train[:, 0 : params.n]
            # y_split = data[:, params.n:].to(dtype=torch.double)
            y_split = torch.abs(data_train[:, params.n :])

            # Zero your gradients for every batch!
            optimizer.zero_grad()

            # Make predictions for this batch
            x_pred = model_URWF(x_split.T, y_split.T, Amatrix)
            # Compute the loss and its gradients
            loss = models.my_loss(x_split.T, x_pred)
            loss.backward()

            # Adjust learning weights
            optimizer.step()

            # Gather data and report
            running_loss += loss.item()
            # if i % 1000 == 999:
            last_loss = running_loss / 1000  # loss per batch

            # print('  batch {} loss: {}'.format(i + 1, last_loss))
            tb_x = epoch_index * len(training_loader) + i + 1
            tb_writer.add_scalar("Loss/train", last_loss, tb_x)
            running_loss = 0.0

        return last_loss

    # print(params.scenario)
    # Initializing in a separate cell so we can easily add more epochs to the same run
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    writer = SummaryWriter("runs/fashion_trainer_{}".format(timestamp))
    epoch_number = 0
    best_vloss = 1_000_000.0
    x_val_pred = model_URWF(x_val.T, y_val.T, Amatrix)
    err_URWF_knot = models.Rel_Er(x_val.T, x_val_pred)
    # print(err_URWF_knot)
    for epoch in range(params.EPOCHS):
        print("EPOCH_URWF {}:".format(epoch_number + 1))
        # Make sure gradient tracking is on, and do a pass over the data
        model_URWF.train(True)
        avg_loss = train_one_epoch_URWF(epoch_number, writer, data_train, data_val)
        running_vloss = 0.0
        # Set the model to evaluation mode, disabling dropout and using population
        # statistics for batch normalization.
        model_URWF.eval()
        # Disable gradient computation and reduce memory consumption.
        with torch.no_grad():
            for i, vdata in enumerate(validation_loader):
                x_split = data_val[:, 0 : params.n]
                y_split = torch.abs(data_val[:, params.n :])
                x_pred = model_URWF(x_split.T, y_split.T, Amatrix)
                vloss = models.my_loss(x_split.T, x_pred)
                running_vloss += vloss

        avg_vloss = running_vloss / (i + 1)
        # print('LOSS train {} valid {}'.format(avg_loss, avg_vloss))

        # Log the running loss averaged per batch
        # for both training and validation
        writer.add_scalars(
            "Training vs. Validation Loss",
            {"Training": avg_loss, "Validation": avg_vloss},
            epoch_number + 1,
        )
        writer.flush()

        # Track best performance, and save the model's state
        if avg_vloss < best_vloss:
            best_vloss = avg_vloss
            model_path = "URWF_{}_{}".format(timestamp, epoch_number)
            # torch.save(model.state_dict(), model_path)
            torch.save(model_URWF.state_dict(), "./models_URWF/" + str(model_path))

        epoch_number += 1

    x_val_pred = model_URWF(x_val.T, y_val.T, Amatrix)
    err_URWF_inf = models.Rel_Er(x_val.T, x_val_pred)
    return err_URWF_knot, err_URWF_inf
