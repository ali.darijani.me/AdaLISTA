import torch
import numpy as np
import matplotlib.pyplot as plt
import math
import generating
import tikzplotlib
from torch.utils.data import DataLoader
from datetime import datetime
from torch.utils.data import DataLoader, SubsetRandomSampler

from torch.utils.tensorboard import SummaryWriter


def tikzplotlib_fix_ncols(obj):
    """
    workaround for matplotlib 3.6 renamed legend's _ncol to _ncols, which breaks tikzplotlib
    """
    if hasattr(obj, "_ncols"):
        obj._ncol = obj._ncols
    for child in obj.get_children():
        tikzplotlib_fix_ncols(child)


class URWF(torch.nn.Module):
    def __init__(
        self,
        cuda_flag,
        n,
        cplx_flag,
        npower_iter,
        T,
        scenario,
        scalar,
        vector,
        matrix,
        tensor,
    ):
        super().__init__()
        self.cuda_flag = cuda_flag
        if torch.cuda.is_available() & self.cuda_flag:
            self.DEVICE = "cuda"
        else:
            self.DEVICE = "cpu"
        self.cplx_flag = cplx_flag
        self.npower_iter = npower_iter
        self.T = T
        self.scenario = scenario

        # self.DEVICE = DEVICE
        self.unit_scalar = torch.nn.Parameter(
            torch.ones(1, device=self.DEVICE), requires_grad=scalar
        )
        self.unit_vector = torch.nn.Parameter(
            torch.ones(T + 1, device=self.DEVICE), requires_grad=vector
        )
        self.unit_matrix = torch.nn.Parameter(
            torch.eye(n, device=self.DEVICE), requires_grad=matrix
        )
        Tensor = torch.zeros(T + 1, n, n, device=self.DEVICE)
        for i in range(T + 1):
            for k in range(n):
                Tensor[i][k][k] = 1.0
        self.unit_tensor = torch.nn.Parameter(Tensor, requires_grad=tensor)

    def forward(self, x, y, Amatrix):
        mu = 0.8 + 0.4 * self.cplx_flag
        m = y.shape[0]
        z0 = torch.randn(x.shape, dtype=x.dtype, device=self.DEVICE)
        z0 = z0 / torch.linalg.norm(z0, dim=0)
        tmp_r = math.sqrt(math.pi / 2) * (1 - self.cplx_flag)
        tmp_c = math.sqrt(4 / math.pi) * self.cplx_flag
        y_m = torch.sum(y, dim=0) / m
        norm_est = (tmp_r + tmp_c) * y_m

        ytr = torch.multiply(y, (torch.abs(y) > 1 * norm_est))  # truncated version

        for i in range(self.npower_iter):
            A_z0 = self._A(Amatrix, z0)
            trunc = torch.multiply(ytr, A_z0)
            z0 = self._Ah(Amatrix, trunc)
            z0 = z0 / torch.linalg.norm(z0, dim=0)
        z0 = norm_est * z0
        z = z0

        for t in range(self.T + 1):
            yz = self._A(Amatrix, z)
            yz_abs = torch.abs(yz)
            first_divide = torch.divide(yz, yz_abs)
            first_multi = torch.multiply(y, first_divide)
            sub = yz - first_multi
            second_multi = self._Ah(Amatrix, sub)
            second_divide = torch.divide(second_multi, m)

            if self.scenario == 0:
                z = z - mu * self.unit_scalar * second_divide
            elif self.scenario == 1:
                z = z - mu * self.unit_vector[t] * second_divide
            elif self.scenario == 2:
                z = z - mu * torch.linalg.matmul(
                    self.unit_matrix.cfloat(), second_divide.cfloat()
                )
            elif self.scenario == 3:
                z = z - mu * torch.linalg.matmul(
                    (
                        torch.linalg.matmul(self.unit_matrix.T, self.unit_matrix)
                    ).cfloat(),
                    second_divide.cfloat(),
                )
            elif self.scenario == 4:
                z = z - mu * self.unit_vector[t] * torch.linalg.matmul(
                    self.unit_matrix.cfloat(), second_divide.cfloat()
                )
            elif self.scenario == 5:
                z = z - mu * self.unit_vector[t] * torch.linalg.matmul(
                    (
                        torch.linalg.matmul(self.unit_matrix.T, self.unit_matrix)
                    ).cfloat(),
                    second_divide.cfloat(),
                )
            elif self.scenario == 6:
                z = z - mu * torch.linalg.matmul(
                    self.unit_tensor[t].cfloat(), second_divide.cfloat()
                )
            elif self.scenario == 7:
                z = z - mu * torch.linalg.matmul(
                    (
                        torch.linalg.matmul(self.unit_tensor[t].T, self.unit_tensor[t])
                    ).cfloat(),
                    second_divide.cfloat(),
                )

        return z, self._distance(z, x)

    def _A(self, Matrix, X):
        return torch.linalg.matmul(Matrix, X)

    def _Ah(self, Matrix, X):
        return torch.linalg.matmul(Matrix.adjoint(), X)

    def _distance(self, x_pred, x):
        N_train = x.shape[1]
        distance = torch.zeros(N_train, device=self.DEVICE)
        for tt in range(N_train):
            X_pred_f = torch.flatten(x_pred[:, tt])
            X_f = torch.flatten(x[:, tt])
            dot_prod = torch.dot(X_f.conj_physical(), X_pred_f)
            angle = torch.angle(dot_prod)
            X_f_norm = torch.linalg.norm(X_f)
            X_pred_f_angle = torch.exp(-1j * angle) * X_pred_f
            diff = X_f - X_pred_f_angle
            distance[tt] = torch.linalg.norm(diff) / X_f_norm
        return distance


def train_URWF(
    x,
    x_val,
    Amatrix,
    y,
    y_val,
    LR,
    cuda_flag,
    cplx_flag,
    npower_iter,
    T,
    SCENARIO,
    scalar,
    vector,
    matrix,
    tensor,
    EPOCHS,
):
    # print("dalam")
    if torch.cuda.is_available() & cuda_flag:
        DEVICE = "cuda"
    else:
        DEVICE = "cpu"
    n = x.shape[0]
    model_RWF = URWF(
        cuda_flag,
        n,
        cplx_flag,
        npower_iter,
        T,
        SCENARIO,
        scalar,
        vector,
        matrix,
        tensor,
    )
    model_URWF = URWF(
        cuda_flag,
        n,
        cplx_flag,
        npower_iter,
        T,
        SCENARIO,
        scalar,
        vector,
        matrix,
        tensor,
    )
    err_RWF_train = np.zeros(EPOCHS)
    err_RWF_test = np.zeros(EPOCHS)
    # print("sambalam")
    for epoch in range(EPOCHS):
        data = torch.cat([x, y], dim=0)
        s_ratio = 0.1
        n = x.shape[0]
        N_total = x.shape[1]
        N_test = int(s_ratio * N_total)
        # m = y.shape[0]
        suffled_indices = torch.randperm(N_total)
        train_indices = suffled_indices[:-N_test]
        data_train = data[:, train_indices]
        test_indices = suffled_indices[-N_test:]
        data_test = data[:, test_indices]
        train_loader = DataLoader(data_train.T, batch_size=2)
        test_loader = DataLoader(data_test.T, batch_size=2)
        running_loss = 0.0
        last_loss = 0.0
        # n = x.shape[0]
        x_split_test = data_test[0:n, :]
        # print(x_split_test.shape)
        y_split_test = torch.abs(data_test[n:, :])
        # print("datenkar:", y_split_test.shape)
        x_pred_test, distance_test = model_URWF(x_split_test, y_split_test, Amatrix)
        # print(x_pred_test.shape, distance_test.shape)
        loss_test = model_URWF._distance(x_pred_test, x_split_test)
        err_RWF_test[epoch] = loss_test.mean()
        # loss_test_untrained = model_RWF._distance(x_pred_test, x_split_test)
        # err_RWF_test[epoch_index] = loss_test_untrained.mean()
        x_split_train = data_train[0:n, :]
        y_split_train = torch.abs(data_train[n:, :])
        x_pred_train, distance_train = model_URWF(x_split_train, y_split_train, Amatrix)
        # print(x_pred_train.shape, distance_train.shape)
        loss_train = model_URWF._distance(x_split_train, x_pred_train)
        err_RWF_train[epoch] = loss_train.mean()

    err_URWF_train = np.zeros(EPOCHS)
    err_URWF_test = np.zeros(EPOCHS)

    optimizer = torch.optim.Adam(model_URWF.parameters(), amsgrad=True, lr=LR)
    # print("ishtekhbakhen")

    def train_one_epoch_URWF(epoch_index, tb_writer):
        data = torch.cat([x, y], dim=0)
        s_ratio = 0.1
        n = x.shape[0]
        N_total = x.shape[1]
        N_test = int(s_ratio * N_total)
        # m = y.shape[0]
        suffled_indices = torch.randperm(N_total)
        train_indices = suffled_indices[:-N_test]
        data_train = data[:, train_indices]
        test_indices = suffled_indices[-N_test:]
        data_test = data[:, test_indices]
        train_loader = DataLoader(data_train.T, batch_size=2)
        test_loader = DataLoader(data_test.T, batch_size=2)
        running_loss = 0.0
        last_loss = 0.0
        # n = x.shape[0]
        x_split_test = data_test[0:n, :]
        # print(x_split_test.shape)
        y_split_test = torch.abs(data_test[n:, :])
        # print("datenkar:", y_split_test.shape)
        x_pred_test, distance_test = model_URWF(x_split_test, y_split_test, Amatrix)
        # print(x_pred_test.shape, distance_test.shape)
        loss_test = model_URWF._distance(x_pred_test, x_split_test)
        err_URWF_test[epoch_index] = loss_test.mean()
        # loss_test_untrained = model_RWF._distance(x_pred_test, x_split_test)
        # err_RWF_test[epoch_index] = loss_test_untrained.mean()
        x_split_train = data_train[0:n, :]
        y_split_train = torch.abs(data_train[n:, :])
        x_pred_train, distance_train = model_URWF(x_split_train, y_split_train, Amatrix)
        # print(x_pred_train.shape, distance_train.shape)
        loss_train = model_URWF._distance(x_split_train, x_pred_train)
        err_URWF_train[epoch_index] = loss_train.mean()
        # loss_train_untrained = model_RWF._distance(x_split_train, x_pred_train)
        # err_RWF_train[epoch_index] = loss_train_untrained.mean()
        # print(
        #     "  loss_train_URWF {} loss_test_URWF: {}".format(
        #         loss_train.mean(), loss_test.mean()
        #     )
        # )
        # Here, we use enumerate(training_loader) instead of
        # iter(training_loader) so that we can track the batch
        # index and do some intra-epoch reporting
        for i, data_train in enumerate(train_loader):
            # Every data instance is an input + label pair
            # inputs, labels = data

            x_train = data_train[:, 0:n].T
            # print("DATEN,:", x_split.shape)
            # y_split = data[:, params.n:].to(dtype=torch.double)
            # y_split = torch.abs(data_train[n:, :])

            y_train = torch.abs(data_train[:, n:]).T
            # print("DATEN,:", y_train.shape)
            # y_split = torch.abs(data_train[:, n:])

            # Zero your gradients for every batch!
            optimizer.zero_grad()

            # Make predictions for this batch
            # print(x_train.shape, y_train.shape)

            x_train_pred, distance_train = model_URWF(x_train, y_train, Amatrix)
            # Compute the loss and its gradients
            loss = model_URWF._distance(x_train_pred, x_train).mean()
            loss.backward()

            # Adjust learning weights
            optimizer.step()

            # Gather data and report
            running_loss += loss.item()
            # if i % 1000 == 999:
            last_loss = running_loss / 1000  # loss per batch

            # print('  batch {} loss: {}'.format(i + 1, last_loss))
            tb_x = epoch_index * len(train_loader) + i + 1
            tb_writer.add_scalar("Loss/train", last_loss, tb_x)
            running_loss = 0.0

        return last_loss, data_train, data_test

    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    writer = SummaryWriter("runs/fashion_trainer_{}".format(timestamp))
    epoch_number = 0
    best_vloss = 1_000_000.0
    x_val_pred = model_URWF(x_val, y_val, Amatrix)
    # err_URWF_knot = model_URWF._distance(x_val, x_val_pred)
    # print(err_URWF_knot)
    # print("nakhtabam")
    for epoch in range(EPOCHS):
        # print("epoch", epoch)
        # print("EPOCH_URWF {}:".format(epoch_number + 1))
        # Make sure gradient tracking is on, and do a pass over the data
        model_URWF.train(True)
        avg_loss, data_train, data_test = train_one_epoch_URWF(epoch_number, writer)
        train_loader = DataLoader(data_train.T, batch_size=2)
        test_loader = DataLoader(data_test.T, batch_size=2)
        running_vloss = 0.0
        # Set the model to evaluation mode, disabling dropout and using population
        # statistics for batch normalization.
        model_URWF.eval()
        # Disable gradient computation and reduce memory consumption.
        with torch.no_grad():
            for i, vdata in enumerate(test_loader):
                x_split_test = data_test[0:n, :]
                y_split_test = torch.abs(data_test[n:, :])
                x_pred, vloss = model_URWF(x_split_test, y_split_test, Amatrix)
                # vloss = model_URWF._distance(x_pred, x_split_test).mean()
                running_vloss += vloss.mean()

        avg_vloss = running_vloss / (i + 1)
        # print('LOSS train {} valid {}'.format(avg_loss, avg_vloss))

        # Log the running loss averaged per batch
        # for both training and validation
        writer.add_scalars(
            "Training vs. Validation Loss",
            {"Training": avg_loss, "Validation": avg_vloss},
            epoch_number + 1,
        )
        writer.flush()

        # Track best performance, and save the model's state
        # if avg_vloss < best_vloss:
        # best_vloss = avg_vloss
        # model_path = "URWF_{}_{}".format(timestamp, epoch_number)
        # torch.save(model.state_dict(), model_path)
        # torch.save(model_URWF.state_dict(), "./models_URWF/" + str(model_path))
        # print(err_URWF_train[epoch])
        # print(err_URWF_test[epoch])
        # print(err_RWF_train[epoch])
        # print(err_RWF_test[epoch])
        # print("##########")
        epoch_number += 1

    x_val_pred, distance_val = model_URWF(x_val, y_val, Amatrix)
    err_URWF_inf = distance_val
    # print(err_URWF_inf.mean())
    # return err_URWF_inf.mean()
    # print(err_URWF_train)
    # print(err_URWF_test)
    # print(err_RWF_train)
    # print(err_RWF_test)
    # plt.rcParams["text.usetex"] = True
    # plt.style.use("ggplot")

    # plt.figure(1)
    plt.semilogy(
        np.arange(0, EPOCHS),
        err_URWF_train,
        color="red",
        label="During Training on Train Data",
    )
    plt.semilogy(
        np.arange(0, EPOCHS),
        err_URWF_test,
        color="blue",
        label="During Training on Test Data",
    )
    plt.semilogy(
        np.arange(0, EPOCHS),
        err_RWF_train,
        color="orange",
        label="Untrained Model on Train Data",
    )
    plt.semilogy(
        np.arange(0, EPOCHS),
        err_RWF_test,
        color="green",
        label="Untrained Model on Test Data",
    )
    plt.xlabel("Epochs")
    plt.ylabel("Average Error")
    plt.title("Average Error vs Epochs")
    plt.legend()
    fig = plt.gcf()
    tikzplotlib_fix_ncols(fig)
    # plt.title("Coded Diffaction Pattern Combined with \n Wirtinger Flow on the Sat Phone Image")
    # plt.grid(True, which="both")
    plt.grid(True)
    import tikzplotlib

    # plt.grid(True)
    # plt.show()

    plt.savefig(
        "./models_URWF/Scenario = "
        + str(SCENARIO)
        + ", Layers = "
        + str(T)
        + ", Epochs = "
        + str(EPOCHS)
        + ", LR = "
        + str(LR)
        + ", Learning_Process.pdf"
    )
    tikzplotlib.save(
        "./models_URWF/Scenario = "
        + str(SCENARIO)
        + ", Layers = "
        + str(T)
        + ", Epochs = "
        + str(EPOCHS)
        + ", LR = "
        + str(LR)
        + ", Learning_Process"
        + ".tex"
    )
    plt.close()
    # print("finished")
    return err_URWF_test[-1]


# for scenario in [0, 1, 2, 3, 4, 5, 6, 7]:
#     x, x_val, Amatrix, y, y_val = generating.gen_x_Amatrix_y(
#         n=64, m=10 * 64, N=100, cplx_flag=1, cuda_flag=1
#     )
#     err = train_URWF(
#         x,
#         x_val,
#         Amatrix,
#         y,
#         y_val,
#         SCENARIO=scenario,
#         LR=1e-3,
#         cuda_flag=1,
#         cplx_flag=1,
#         npower_iter=30,
#         T=30,
#         scalar=True,
#         vector=True,
#         matrix=True,
#         tensor=True,
#         EPOCHS=50,
#     )
# print("test error is:", err)
