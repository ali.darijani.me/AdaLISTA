import torch
import math


def gen_x_Amatrix_y(n, m, N, cplx_flag, cuda_flag):
    """create x, y, and Amatrix"""
    # Using CUDA if possible and if wanted by the user
    if torch.cuda.is_available() & cuda_flag:
        DEVICE = "cuda"
    else:
        DEVICE = "cpu"
    # making the training singals
    x_1 = torch.randn((n, N), dtype=torch.double, device=DEVICE)
    x_2 = torch.randn((n, N), dtype=torch.double, device=DEVICE) * 1j * cplx_flag
    x = x_1 + x_2
    # making the validation signals
    x_1_val = torch.randn((n, N), dtype=torch.double, device=DEVICE)
    x_2_val = torch.randn((n, N), dtype=torch.double, device=DEVICE) * 1j * cplx_flag
    x_val = x_1_val + x_2_val
    # making the sampling vectors(Linear Operator)
    Amatrix_1 = torch.randn((m, n), dtype=torch.double, device=DEVICE)
    Amatrix_2 = torch.randn((m, n), dtype=torch.double, device=DEVICE) * 1j * cplx_flag
    scale = math.sqrt(2) ** cplx_flag
    Amatrix = (Amatrix_1 + Amatrix_2) / scale

    # making the measurements data
    # y =|A(x)| Element-wise absolute value of a linear operator
    y = torch.abs(torch.linalg.matmul(Amatrix, x))
    # making the validation measurements data
    # y =|A(x)| Element-wise absolute value of a linear operator
    y_val = torch.abs(torch.linalg.matmul(Amatrix, x_val))
    return x, x_val, Amatrix, y, y_val

    # torch.save(x, 'x.pt')
    # torch.save(x_val, 'x_val.pt')
    # torch.save(Amatrix, 'Amatrix.pt')
    # torch.save(y, 'y.pt')
    # torch.save(y_val, 'y_val.pt')
