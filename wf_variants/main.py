import tikzplotlib
import wf_variants
import torch
import numpy as np
import math
import matplotlib.pyplot as plt
from pprint import pprint
import time
from datetime import timedelta
start = time.time()
def tikzplotlib_fix_ncols(obj):
    """
    workaround for matplotlib 3.6 renamed legend's _ncol to _ncols, which breaks tikzplotlib
    """
    if hasattr(obj, "_ncols"):
        obj._ncol = obj._ncols
    for child in obj.get_children():
        tikzplotlib_fix_ncols(child)
# def tikzplotlib_fix_ncols(obj):
#     """
#     workaround for matplotlib 3.6 renamed legend's _ncol to _ncols, which breaks tikzplotlib
#     """
#     if hasattr(obj, "_ncols"):
#         obj._ncol = obj._ncols
#     for child in obj.get_children():
#         tikzplotlib_fix_ncols(child)
# Python using Pytorch version of https://github.com/hubevan/reshaped-Wirtinger-flow/blob/master/exampleRWF.m
# Example of the RWF and IRWF algorithm under 1D Gaussian designs
# The code below is adapted from implementation of the TWF desinged by Y. Chen and E. Candes, Wirtinger Flow algorithm designed and implemented by E. Candes, X. Li, and M. Soltanolkotabi
print("main programm started")



A,x,y = wf_variants.generator()

wf_err = wf_variants.wf(A,x,y)
twf_err = wf_variants.twf(A,x,y)
# itwf_err = wf_variants.itwf(A,x,y)
rwf_err = wf_variants.rwf(A,x,y)
irwf_err = wf_variants.irwf(A,x,y,batch= 1)
imrwf_err = wf_variants.irwf(A,x,y,batch= 64)

plt.semilogy(np.arange(0, np.size(wf_err)), wf_err, '-r',label="WF")
plt.semilogy(np.arange(0, np.size(twf_err)), twf_err, '-m',label="TWF")
# plt.semilogy(np.arange(0, np.size(itwf_err)), itwf_err, '-b')
plt.semilogy(np.arange(0, np.size(rwf_err)), rwf_err, '-c',label="RWF")
plt.semilogy(np.arange(0, np.size(irwf_err)), irwf_err, '-g',label="IRWF")
plt.semilogy(np.arange(0, np.size(imrwf_err)), imrwf_err, '-y',label="IMRWF")
plt.xlabel('Iteration')
plt.ylabel('Relative error (log10)')
plt.title('Wirtinger Flow Variants')
plt.legend()
# fig = plt.figure()
fig = plt.gcf()
tikzplotlib_fix_ncols(fig)
tikzplotlib.save("wf_variants.tex")

# plt.savefig('Wirtinger Flow Variants.pdf')
# tikzplotlib.save("wf_variants.tex")
print("misson accomplished in:")
end = time.time()
elapsed_time = end - start
print("the whole programm took:")
print(str(timedelta(seconds=elapsed_time)),"(HH:MM:SS)")
print("#################")


   