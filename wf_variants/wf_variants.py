import torch
import numpy as np
import math
import time
# import pathlib
import datetime
from datetime import timedelta

# Set Parameters
params = dict()
params['n2']          = 1
params['n1']          = 64  # signal dimension
params['m']           = 10 * params['n1']  # number of measurements
params['cplx_flag']   = 1  # real: cplx_flag = 0;  complex: cplx_flag = 1;
params['grad_type']   = 'TWF_Poiss'  # 'TWF_Poiss': Poisson likelihood
params['T']           = 800  # number of iterations
params['npower_iter'] = 30  # number of power iterations
npower_iter = params['npower_iter']  # Number of power iterations
tau0 = 330
n           = params['n1']
m           = params['m']
cplx_flag	= params['cplx_flag']  # real-valued: cplx_flag = 0;  complex-valued: cplx_flag = 1;
npower_iter = params['npower_iter']  # Number of power iterations
mu = 0.8+0.4*cplx_flag 



def A_times_x(Amatrix,X):
    result = torch.linalg.matmul(Amatrix, X)
    return result

def Ah_times_x(Amatrix,Z):
    Ah = Amatrix.adjoint()
    result = torch.linalg.matmul(Ah, Z)
    return result

def generator():
    start = time.time()
    print("#################")
    print("allocating x,A,y ...")
    print("n =",n,",m =",m,",complex flag =",cplx_flag)
    x= torch.randn((n,1), dtype=torch.double)+torch.randn((n,1), dtype=torch.double)*1j
    Amatrix = (torch.randn((m,n), dtype=torch.double)+torch.randn((m,n), dtype=torch.double)*1j)/math.sqrt(2)**cplx_flag
    y = torch.abs(A_times_x(Amatrix,x))
    print("finished")
    end = time.time()
    elapsed_time = end - start
    print("this operation took:")
    print(str(timedelta(seconds=elapsed_time)),"(HH:MM:SS)")
    print("#################")
    return Amatrix,x,y

####################################################
####################################################
############## Original Wirtinger FLow #############
####################################################
####################################################

def wf(Amatrix,x,y):
    start = time.time()
    print("#################")
    print("Wirtinger Flow Algorithm is being deployed ...")
    print("n =",n,",m =",m,",complex flag =",cplx_flag,",power iteration =",npower_iter,",Total Iteration =",params['T'])
    print("tau =",tau0)
    ITR = np.arange(params['T']+1).reshape((params['T']+1, 1))
    Relerrs = np.zeros((params['T']+1, 1))
    z0 = torch.randn((n, params['n2']), dtype=torch.cdouble)
    z0 = z0/torch.linalg.norm(z0)
    for tt in range(npower_iter):
        z0 = Ah_times_x(Amatrix,torch.multiply(y, (A_times_x(Amatrix,z0))))
        z0 = z0/torch.linalg.norm(z0)
    normest = math.sqrt(torch.sum(y))/params['m']
    z0 = normest * z0
    z = z0
    Relerrs[0] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)
    normest = math.sqrt(torch.sum(y**2)/params['m'])
    for tt in range(params['T']+1):
        yz = Amatrix @ z
        grad  = 1/m * Ah_times_x( Amatrix, torch.multiply(abs(yz)**2-y**2, yz))
        z = z - (min(1-math.exp(-tt/tau0), 0.2))/normest**2 * grad    
        Relerrs[tt] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)

    err = np.concatenate((ITR, Relerrs), axis=1)    
    np.savetxt("wf_err.dat",err)
    print("finished")
    end = time.time()
    elapsed_time = end - start
    print("this operation took:")
    print(str(timedelta(seconds=elapsed_time)),"(HH:MM:SS)")
    print("#################")
    return Relerrs

####################################################
####################################################
############# Truncated Wirtinger FLow #############
####################################################
####################################################

def twf(Amatrix,x,y):
    start = time.time()

    print("#################")
    print("Truncated Wirtinger Flow Algorithm is being deployed ...")
    print("n =",n,",m =",m,",complex flag =",cplx_flag,",power iteration =",npower_iter,",Total Iteration =",params['T'])
    mu = 0.0
    tau0 = 0
    if params['grad_type'] == 'TWF_Poiss':
        mu = 0.2 
        print("grad type =",params['grad_type'])
        print("mu =",mu)
    elif params['grad_type'] == 'WF_Poiss':
        tau0 = 330
        print("grad type =",params['grad_type'])
        print("tau =",tau0)
    y = y**2



    alpha_y = 3
    alpha_ub = 5
    alpha_lb = 0.3
    alpha_h = 5
    print("alpha_y =",alpha_y,",alpha_ub =",alpha_ub,",alpha_lb =",alpha_lb,",alpha_h =",alpha_h)
    ITR = np.arange(params['T']+1).reshape((params['T']+1, 1))
    Relerrs = np.zeros((params['T']+1, 1))
    z0 = torch.randn((n, params['n2']), dtype=torch.cdouble)
    z0 = z0/torch.linalg.norm(z0)
    normest = math.sqrt(torch.sum(y))/params['m']
    for tt in range(npower_iter):
        mask = torch.multiply(1, (torch.abs(y) > alpha_y**2 * normest**2))
        ytr = torch.mul(y , mask)
        z0 = Ah_times_x(Amatrix,torch.multiply(ytr, (A_times_x(Amatrix,z0))))
        z0 = z0/torch.linalg.norm(z0)
    z0 = normest * z0
    z = z0
    Relerrs[0] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)
    for tt in range(params['T']+1):
        yz = Amatrix @ z
        Kt = 1/m * torch.linalg.norm(torch.abs(yz)**2-y,1)
        if params['grad_type'] == 'TWF_Poiss':
            temp_ub = torch.abs(yz)/torch.linalg.norm(z)
            Eub = torch.multiply(1, (torch.abs(temp_ub) <= alpha_ub))
            temp_lb = torch.abs(yz)/torch.linalg.norm(z)
            Elb = torch.multiply(1, (torch.abs(temp_lb) >= alpha_lb))
            temp_h = torch.abs(y-torch.abs(yz)**2)
            thre = (alpha_h * Kt / torch.linalg.norm(z)) * torch.linalg.norm(yz)
            Eh = torch.multiply(1, (torch.abs(temp_h) <= thre))
            temp = torch.mul(2* torch.div(torch.abs(yz)**2-y,torch.abs(yz)**2), yz)
            temp = torch.mul(temp,Eub)
            temp = torch.mul(temp,Elb)
            temp = torch.mul(temp,Eh) 
        
            grad = 1/m * Ah_times_x(Amatrix,temp)

        if params['grad_type'] == 'TWF_Poiss': 
            z = z - mu * grad
        elif params['grad_type'] == 'WF_Poiss': 
            z = z - (min(1-math.exp(-tt/tau0), 0.2)) * grad 
        
        Relerrs[tt] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)
    
    err = np.concatenate((ITR, Relerrs), axis=1)    
    np.savetxt("twf_err.dat",err)
    print("finished")
    end = time.time()
    elapsed_time = end - start
    print("this operation took:")
    print(str(timedelta(seconds=elapsed_time)),"(HH:MM:SS)")
    print("#################")
    return Relerrs


####################################################
####################################################
############ Reshaped Wirtinger FLow ###############
####################################################
####################################################

def rwf(Amatrix,x,y):
    start = time.time()
    print("#################")
    print("Reshaped Wirtinger Flow Algorithm is being deployed ...")
    print("n =",n,",m=",m,",complex flag =",cplx_flag,",power iteration =",npower_iter,",Total Iteration =",params['T'])
    print("mu =",mu)
    ITR = np.arange(params['T']+1).reshape((params['T']+1, 1))
    Relerrs = np.zeros((params['T']+1, 1))
    z0 = torch.randn((n, params['n2']), dtype=torch.cdouble)
    torch.linalg.norm(z0)
    normest = (math.sqrt(math.pi/2)*(1-params['cplx_flag'])+math.sqrt(4/math.pi)*params['cplx_flag'])*torch.sum(y)/params['m']
    ytr = torch.multiply(y, (torch.abs(y) > 1 * normest))
    for tt in range(npower_iter):
        z0 = Ah_times_x(Amatrix,torch.multiply(ytr, (A_times_x(Amatrix,z0))))
        z0 = z0/torch.linalg.norm(z0)
    z0 = normest * z0
    z = z0
    Relerrs[0] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)
    for t in range(params['T']+1):
        yz = Amatrix @ z
        yz_abs=torch.abs(yz)
        first_divide=torch.divide(yz,yz_abs)
        first_multi=torch.multiply(y,first_divide)
        sub=yz-first_multi
        second_multi=Ah_times_x(Amatrix,sub)
        second_divide=torch.divide(second_multi,params['m'])
        z = z - mu*second_divide
        Relerrs[t] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)
    
    err = np.concatenate((ITR, Relerrs), axis=1)    
    np.savetxt("rwf_err.dat",err)
    print("finished")
    end = time.time()
    elapsed_time = end - start
    print("this operation took:")
    print(str(timedelta(seconds=elapsed_time)),"(HH:MM:SS)")
    print("#################")
    return Relerrs

####################################################
####################################################
##### Incrementally Reshaped Wirtinger FLow ########
####################################################
####################################################

def irwf(Amatrix,x,y,batch):
    start = time.time()
    print("#################")
    print("Incementally Reshaped Wirtinger Flow Algorithm is being deployed ...")
    print("n =",n,",m =",m,",complex flag =",cplx_flag,",power iteration =",npower_iter,",Total Iteration =",params['T'])
    print("mu =",mu,",batch =",batch)
    ITR = np.arange(params['T']+1).reshape((params['T']+1, 1))
    Relerrs = np.zeros((params['T']+1, 1))
    z0 = torch.randn((n, params['n2']), dtype=torch.cdouble)
    torch.linalg.norm(z0)
    normest = (math.sqrt(math.pi/2)*(1-params['cplx_flag'])+math.sqrt(4/math.pi)*params['cplx_flag'])*torch.sum(y)/params['m']
    ytr = torch.multiply(y, (torch.abs(y) > 1 * normest))
    for tt in range(npower_iter):
        z0 = Ah_times_x(Amatrix,torch.multiply(ytr, (A_times_x(Amatrix,z0))))
        z0 = z0/torch.linalg.norm(z0)
    z0 = normest * z0
    z = z0
    Relerrs[0] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)
    sgd = params['m']
    # batch = params['n1']
    
    for t in range(1, params['T']+1):
        for i in np.arange(0, sgd-batch+1, batch):
            Asub = Amatrix[i:i+batch, :]
            Asubh = Asub.H
            ysub = y[i:i+batch]
            Asubz = Asub @ z
            z = z - (Asubh@(torch.divide(Asubz-torch.multiply(ysub, torch.divide(Asubz, torch.abs(Asubz))), params['n1'])))
        Relerrs[t] = torch.linalg.norm(x - torch.exp(-1j*torch.angle(torch.trace(x.H*z))) * z)/torch.linalg.norm(x)
    
    err = np.concatenate((ITR, Relerrs), axis=1)    
    np.savetxt("irwf_err_"+str(batch)+".dat",err)
    print("finished")
    end = time.time()
    elapsed_time = end - start
    print("this operation took:")
    print(str(timedelta(seconds=elapsed_time)),"(HH:MM:SS)")
    print("#################")
    return Relerrs
